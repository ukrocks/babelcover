import React, { Component } from 'react';
import { Card, Grid, Button } from 'semantic-ui-react';
import Layout from '../../components/Layout';
import Insurance from '../../ethereum/insurance';
import ClaimsContract from '../../ethereum/Claims';
import factory from '../../ethereum/factory';
import web3 from '../../ethereum/web3';
import ClaimRequest from '../../ethereum/ClaimRequests';
import ClaimForm from '../../components/ClaimForm';
import { Link } from '../../routes';

class InsuranceShow extends Component {
  static async getInitialProps(props) {
    const insurance = Insurance(props.query.address);

    const claims = await factory.methods.ClaimsInterface().call();

    const claimsInterface = ClaimsContract(claims);

    console.log("searching requests for "+props.query.id);

    const requests = await claimsInterface.methods.getRequestsForUser(props.query.id).call();

    console.log(requests);
    return {
      id: props.query.id,
      requests : requests
    };
  }

  renderCards() {
    const items = this.props.requests.map(address => {
      return {
        header: address,
        description: (
          <Link route={`/claimsApprover/approve/${address}`}>
            <a>Approve</a>
          </Link>
        ),
        fluid: true
      };
    });

    return <Card.Group items={items} />;
  }

  render() {
    return (
      <Layout>
        <h3>Insurance Show</h3>
        <Grid>
          <Grid.Row>
            <Grid.Column width={10}>{this.renderCards()}</Grid.Column>
          </Grid.Row>
        </Grid>
      </Layout>
    );
  }
}

export default InsuranceShow;
