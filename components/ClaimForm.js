import React, { Component } from 'react';
import { Form, Input, Message, Button } from 'semantic-ui-react';
import Insurance from '../ethereum/insurance';
import factory from '../ethereum/factory';
import claimInterface from '../ethereum/Claims';
import web3 from '../ethereum/web3';
import { Router } from '../routes';

class ClaimForm extends Component {
  state = {
    value: '',
    errorMessage: '',
    loading: false
  };

  onSubmit = async event => {
    event.preventDefault();

    const claimContract = await factory.methods.ClaimsInterface().call();

    const claim = claimInterface(claimContract);

    this.setState({ loading: true, errorMessage: '' });

    try {
      const res = await claim.methods.requestClaim(1, this.state.value, ""+this.props.address).call();

      console.log("res: "+ res);
      //Router.replaceRoute(`/insurance/${this.props.address}`);
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }

    this.setState({ loading: false, value: '' });
  };

  render() {

    console.log(this.props.data.policyState);

    if(this.props.data.policyState != 2){
      return (
        <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
          <Form.Field>
            <label>Enter the claim amount</label>
            <Input
              value={this.state.value}
              onChange={event => this.setState({ value: event.target.value })}
              label="wei"
              labelPosition="right"
            />
          </Form.Field>

          <Message error header="Oops!" content={this.state.errorMessage} />
          <Button primary loading={this.state.loading}>
            Request Claim!
          </Button>
        </Form>
      );
    }
    else {
      return null;
    }
  }
}

export default ClaimForm;
